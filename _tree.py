from flask import abort
from DBCommunication import DBCommunication

dbcom = DBCommunication(abort)


def validate(name):
    import re

    if not hasattr(validate, "filter_name"):
        re1 = re.compile(r"^[^\s\x7f\x80]", re.U)
        re2 = re.compile(r"^[a-zA-Zа-яА-Я]+( [a-zA-Zа-яА-Я]+)*$", re.U)
        re3 = re.compile(r"[^\s\x7f\x80]$", re.U)
        setattr(validate, 'filter_name', lambda e: re1.search(e) and re2.search(e) and re3.search(e))
    return validate.filter_name(name)


def create(node):
    name = node.get("name")
    parent_id = node.get("parent_id")

    parent = dbcom.get_node(parent_id)
    if not parent:
        abort(
            409,
            f"Incorrect parent id: {parent_id}"
        )

    if not validate(name):
        abort(
            409,
            f"Incorrect name for node: '{name}'"
        )

    new_node = dbcom.register_node(name, parent_id)
    dbcom.add_child(parent, new_node)
    return new_node


def get_one(node_id: str):
    node = dbcom.get_node(node_id)
    if not node:
        abort(
            409,
            f"Try to access nonexistent node: {node_id}"
        )
    return node


def get_children(node_id: str):
    return list(dbcom.iterate_children(node_id))


def delete(node_id: str):
    return dbcom.unregister_node(node_id)


if __name__ == "__main__":
    create({"name": "f", "parent_id": "1"})
    create({"name": "a", "parent_id": "2"})
    create({"name": "b", "parent_id": "2"})
    create({"name": "c", "parent_id": "2"})
    create({"name": "v", "parent_id": "2"})
    create({"name": "hello", "parent_id": "1"})
