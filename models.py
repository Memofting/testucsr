from config import db, ma, NAME_LEN, ID_LEN


class Node(db.Model):
    __tablename__ = 'tree'
    name = db.Column(db.String(NAME_LEN))

    # todo: change internal stored values from string to int or long
    node_id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.String(ID_LEN))
    children_id = db.Column(db.String(ID_LEN))
    left_node_id = db.Column(db.String(ID_LEN))


class NodeSchema(ma.ModelSchema):
    class Meta:
        model = Node
        sqla_session = db.session
