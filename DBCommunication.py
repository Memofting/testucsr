from flask import abort
from config import db
from models import Node, NodeSchema


class DBCommunication:
    _ROOT_ID = "1"

    def __init__(self, abort_function: abort):
        from functools import partial

        self.conflict_abort = partial(abort_function, 409)
        self.not_allowed_abort = partial(abort_function, 405)

    def get_node(self, node_id: str) -> dict:
        node = Node.query.filter(Node.node_id == node_id).one_or_none()
        node_schema = NodeSchema()
        return node_schema.dump(node).data

    def register_node(self, name: str, parent_id: str) -> dict:
        '''
        Create node in database and connect it with parent
        '''
        node = {'name': name, 'parent_id': parent_id}

        schema = NodeSchema()
        for child in self.iterate_children(parent_id):
            if child["name"] == name:
                self.conflict_abort(
                    f"There is another node with same name: {child['node_id']}"
                )

        new_node = schema.load(node, session=db.session).data

        db.session.add(new_node)
        db.session.commit()

        return schema.dump(new_node).data

    def unregister_node(self, node_id: str) -> dict:
        if node_id == self._ROOT_ID:
            self.not_allowed_abort(
                f"Can't delete root node: {node_id}"
            )

        node = self.get_node(node_id)
        if not node:
            self.conflict_abort(
                f"Try to delete nonexistent node: {node_id}"
            )
        parent = self.get_node(node['parent_id'])

        self._remove_child(parent, node)
        self._delete_node(node_id)

        return node

    def iterate_children(self, node_id: str):
        node = self.get_node(node_id)
        if not node:
            self.conflict_abort(
                f"Try to iterate through nonexistent node: {node_id}"
            )

        first = child = self.get_node(node['children_id'])
        if child:
            while True:
                yield child
                child = self.get_node(child['left_node_id'])
                if not child or child['node_id'] == first['node_id']:
                    return

    def add_child(self, parent: dict, new_child: dict):
        if parent.get('children_id') is None:
            self._update_node(parent.get('node_id'),
                         children_id=new_child.get('node_id'))
            self._update_node(new_child.get('node_id'),
                         left_node_id=new_child.get('node_id'))
        else:
            first_child = self.get_node(parent.get('children_id'))
            self._add_child(first_child, new_child)

    def _update_node(self, node_id, **kwargs) -> dict:
        '''
        Load node to database and return its view.
        '''
        update_node = self.get_node(node_id)
        if update_node:
            schema = NodeSchema()
            update = schema.load(update_node, session=db.session).data

            # rewrite node in database
            update.id = update_node.get('node_id')
            for k, v in kwargs.items():
                setattr(update, k, v)

            db.session.merge(update)
            db.session.commit()

            # # return updated node in the response
            data = schema.dump(update).data

            return data

    def _add_child(self, first_node: dict, next_node: dict):
        temp = first_node['left_node_id']
        first_node['left_node_id'] = next_node['node_id']
        next_node['left_node_id'] = temp

        ln = self._update_node(**first_node)
        nn = self._update_node(**next_node)

        return ln, nn

    def _remove_child(self, parent: dict, possible_child: dict):
        if parent.get('children_id') is None:
            return

        node_id = str(possible_child['node_id'])
        children = self.iterate_children(str(parent["node_id"]))

        for child in children:
            if child['left_node_id'] == node_id:
                break
        else:
            return
        if child['node_id'] == possible_child['node_id']:
            self._update_node(child['parent_id'], children_id=None)
        elif node_id == parent['children_id']:
            self._update_node(child['parent_id'], children_id=child['node_id'])
        else:
            child['left_node_id'] = possible_child['left_node_id']
            self._update_node(child['node_id'])

    def _delete_node(self, node_id: str):
        for child in self.iterate_children(node_id):
            self.unregister_node(child['node_id'])

        node = Node.query.filter(Node.node_id == node_id).one_or_none()

        if node is not None:
            db.session.delete(node)
            db.session.commit()


