import os
from config import db
from models import Node


root = \
    {
        "name": "",
        "parent_id": None,
        "children_id": None,
        "left_node_id": None
    }


if os.path.exists('tree.db'):
    os.remove('tree.db')

db.create_all()

_r = Node(**root)
db.session.add(_r)

db.session.commit()
